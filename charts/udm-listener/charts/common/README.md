# common

Common utilities and fragments for Helm charts

- **Version**: 0.2.0
- **Type**: library
- **AppVersion**: 1.16.0
-

## Introduction

This chart bundles commonly used functionality to be used in our Helm charts.

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.2.2 |

