{{- /*
common.deployment will render a Deployment manifest and apply overrides if provided.

Arguments are passed as a dict with the following keys:

- top: The top level context

- overrides: (optional) Overrides to apply.

*/}}

{{- define "common.deployment" -}}
{{- include "common.utils.merge" (set . "base" "common.deployment.tpl") }}
{{- end }}


{{- /*
common.deployment.tpl will render a Deployment manifest.

Arguments are passed as a dict with the following keys:

- top: The top level context

*/}}
{{- define "common.deployment.tpl" }}
apiVersion: {{ include "common.capabilities.deployment.apiVersion" .top }}
kind: Deployment
metadata:
  name: {{ include "common.names.fullname" .top }}
  labels:
    {{- include "common.labels.standard" .top | nindent 4 }}
spec:
  replicas: {{ .top.Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "common.labels.matchLabels" .top | nindent 6 }}
  template:
    metadata:
      {{- with .top.Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "common.labels.standard" .top | nindent 8 }}
    spec:
      {{- with .top.Values.affinity }}
      affinity:
        {{ toYaml . | nindent 8 | trim }}
      {{- end }}
      {{- with .top.Values.tolerations }}
      tolerations:
        {{ toYaml . | nindent 8 | trim }}
      {{- end }}
      {{- with .top.Values.nodeSelector }}
      nodeSelector:
        {{ toYaml . | nindent 8 | trim }}
      {{- end }}
      # TODO: Remove `Values.imagePullSecrets` once it has been replaced by `image.pullSecrets` everywhere.
      {{- with (coalesce .top.Values.image.pullSecrets .top.Values.imagePullSecrets) }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .top.Values.podSecurityContext }}
      securityContext:
        {{ toYaml . | nindent 8 | trim }}
      {{- end }}
      containers:
        - name: {{ include "common.names.name" .top }}
          securityContext:
            {{- toYaml .top.Values.securityContext | nindent 12 }}
          {{- with .top.Values.image }}
          image: "{{ if .registry }}{{ .registry }}/{{ end }}{{ .repository }}{{ if .sha256 }}@sha256:{{ .sha256 }}{{ else }}:{{ .tag }}{{ end }}"
          imagePullPolicy: {{ .pullPolicy }}
          {{- end }}
          envFrom:
            - configMapRef:
                name: {{ include "common.names.fullname" .top }}
          volumeMounts:
            {{- if .top.Values.mountSecrets }}
            - name: "secrets"
              # TODO: conflict with /run/secrets, should use a namespace
              mountPath: "/var/secrets"
            {{- end }}
          ports:
            {{- range $key, $value := .top.Values.service.ports }}
            - name: {{ $key }}
              containerPort: {{ $value.containerPort }}
              protocol: {{ $value.protocol }}
            {{- end }}
          {{- if .top.Values.probes.liveness.enabled }}
          livenessProbe:
            tcpSocket:
              port: http
            initialDelaySeconds: {{ .top.Values.probes.liveness.initialDelaySeconds }}
            timeoutSeconds: {{ .top.Values.probes.liveness.timeoutSeconds }}
            periodSeconds: {{ .top.Values.probes.liveness.periodSeconds }}
            failureThreshold: {{ .top.Values.probes.liveness.failureThreshold }}
            successThreshold: {{ .top.Values.probes.liveness.successThreshold }}
          {{- end }}
          {{- if .top.Values.probes.readiness.enabled }}
          readinessProbe:
            tcpSocket:
              port: http
            initialDelaySeconds: {{ .top.Values.probes.readiness.initialDelaySeconds }}
            timeoutSeconds: {{ .top.Values.probes.readiness.timeoutSeconds }}
            periodSeconds: {{ .top.Values.probes.readiness.periodSeconds }}
            failureThreshold: {{ .top.Values.probes.readiness.failureThreshold }}
            successThreshold: {{ .top.Values.probes.readiness.successThreshold }}
          {{- end }}
          resources:
            {{- toYaml .top.Values.resources | nindent 12 }}
      volumes:
        {{- if .top.Values.mountSecrets }}
        - name: "secrets"
          secret:
            secretName: {{ include "common.names.fullname" .top | quote }}
        {{- end }}
{{- end }}
