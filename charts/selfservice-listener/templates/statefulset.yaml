# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: 2024 Univention GmbH

---

apiVersion: "apps/v1"
kind: "StatefulSet"
metadata:
  name: '{{ include "common.names.fullname" . }}'
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
spec:
  serviceName: {{ include "common.names.fullname" . }}
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "common.labels.matchLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        checksum/config: '{{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}'
        checksum/secret: '{{ include (print $.Template.BasePath "/secret.yaml") . | sha256sum }}'
        {{- with .Values.podAnnotations }}
          {{- toYaml . | nindent 8 }}
        {{- end }}
      labels:
        {{- include "common.labels.matchLabels" . | nindent 8 }}
    spec:
      {{- with .Values.image.pullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: '{{ .Chart.Name }}'
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: '{{ if .Values.image.selfserviceListener.registry }}{{ .Values.image.selfserviceListener.registry }}/{{ end }}{{ .Values.image.selfserviceListener.repository }}{{ if .Values.image.selfserviceListener.sha256 }}@sha256:{{ .Values.image.selfserviceListener.sha256 }}{{ else }}:{{ .Values.image.selfserviceListener.tag }}{{ end }}'
          imagePullPolicy: '{{ .Values.image.pullPolicy }}'
          envFrom:
            - configMapRef:
                name: '{{ include "common.names.fullname" . }}'
          volumeMounts:
            - name: "secrets"
              mountPath: "/var/secrets"
            - name: "data"
              mountPath: "/var/lib/univention-directory-listener"
            - name: "shared"
              mountPath: "/var/cache/listener"
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
        - name: '{{ .Chart.Name }}-invitation'
          # command: ["/bin/bash"]
          # args: [sleep", "60000"]
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: '{{ if .Values.image.selfserviceInvitation.registry }}{{ .Values.image.selfserviceInvitation.registry }}/{{ end }}{{ .Values.image.selfserviceInvitation.repository }}{{ if .Values.image.selfserviceInvitation.sha256 }}@sha256:{{ .Values.image.selfserviceInvitation.sha256 }}{{ else }}:{{ .Values.image.selfserviceInvitation.tag }}{{ end }}'
          imagePullPolicy: '{{ .Values.image.pullPolicy }}'
          envFrom:
            - configMapRef:
                name: '{{ include "common.names.fullname" . }}'
          volumeMounts:
            - name: "secrets"
              mountPath: "/var/secrets"
            - name: "shared"
              mountPath: "/var/cache/listener"
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      initContainers:
        - name: wait-for-ldap
          command: ["/bin/bash", "/scripts/wait-for-ldap.sh"]
          {{ with .Values.image.waitForDependency }}
          image: "{{ if .registry }}{{ .registry }}/{{ end }}{{ .repository }}{{ if .sha256 }}@sha256:{{ .sha256 }}{{ else }}:{{ .tag }}{{ end }}"
          imagePullPolicy: {{ .pullPolicy }}
          {{ end }}
          envFrom:
            - configMapRef:
                name: {{ include "common.names.fullname" . }}
          volumeMounts:
            - name: "secrets"
              mountPath: "/var/secrets"
            - name: "wait-for-ldap"
              mountPath: "/scripts"
          resources:
            {{- toYaml .Values.resourcesWaitForDependency | nindent 12 }}
      volumes:
      - name: "secrets"
        secret:
          secretName: {{ include "common.names.fullname" . | quote }}
      - name: wait-for-ldap
        configMap:
          name: {{ include "common.names.fullname" . }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
  volumeClaimTemplates:
  - metadata:
      name: "data"
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: "1Gi"
  - metadata:
      name: "shared"
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: "1Gi"

...
