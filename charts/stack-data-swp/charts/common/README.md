# common

Common utilities and fragments for Helm charts

- **Version**: 0.3.0
- **Type**: library
- **AppVersion**:
-

## Introduction

This chart bundles commonly used functionality to be used in our Helm charts.

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.2.2 |

