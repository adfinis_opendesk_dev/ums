# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: 2024 Univention GmbH

---
apiVersion: {{ include "common.capabilities.deployment.apiVersion" . }}
kind: Deployment
metadata:
  name: {{ include "common.names.fullname" . }}
  namespace: {{ include "common.names.namespace" . | quote }}
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
    {{- if .Values.additionalLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.additionalLabels "context" . ) | nindent 4 }}
    {{- end }}
  {{- if .Values.additionalAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.additionalAnnotations "context" . ) | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy: {{- include "common.tplvalues.render" (dict "value" .Values.updateStrategy "context" .) | nindent 4 }}
  selector:
    matchLabels:
      {{- include "common.labels.matchLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        checksum/configmap: {{ include (print .Template.BasePath "/configmap.yaml") . | sha256sum }}
        {{- if .Values.podAnnotations }}
        {{- include "common.tplvalues.render" (dict "value" .Values.podAnnotations "context" .) | nindent 8 }}
        {{- end }}
      labels:
        {{- include "common.labels.standard" . | nindent 8 }}
    spec:
      {{- if or .Values.imagePullSecrets .Values.global.imagePullSecrets  }}
      imagePullSecrets:
        {{- range .Values.global.imagePullSecrets }}
        - name: "{{ . }}"
        {{- end }}
        {{- range .Values.imagePullSecrets }}
        - name: "{{ . }}"
        {{- end }}
      {{- end }}
      {{- if .Values.affinity }}
      affinity: {{- include "common.tplvalues.render" (dict "value" .Values.affinity "context" .) | nindent 8 }}
      {{- end }}
      {{- if .Values.tolerations }}
      tolerations: {{- include "common.tplvalues.render" (dict "value" .Values.tolerations "context" .) | nindent 8 }}
      {{- end }}
      {{- if .Values.topologySpreadConstraints }}
      topologySpreadConstraints: {{- include "common.tplvalues.render" (dict "value" .Values.topologySpreadConstraints "context" .) | nindent 8 }}
      {{- end }}
      {{- if .Values.nodeSelector }}
      nodeSelector: {{- include "common.tplvalues.render" (dict "value" .Values.nodeSelector "context" .) | nindent 8 }}
      {{- end }}
      {{- if .Values.podSecurityContext.enabled }}
      securityContext: {{- omit .Values.podSecurityContext "enabled" | toYaml | nindent 8 }}
      {{- end }}
      {{- if .Values.serviceAccount.create }}
      serviceAccountName: {{ include "common.names.fullname" . }}
      {{- end }}
      {{- if .Values.terminationGracePeriodSeconds }}
      terminationGracePeriodSeconds: {{ .Values.terminationGracePeriodSeconds }}
      {{- end }}
      containers:
        - name: "umc-server"
          {{- if .Values.containerSecurityContext.enabled }}
          securityContext: {{- omit .Values.containerSecurityContext "enabled" | toYaml | nindent 12 }}
          {{- end }}
          image: {{ include "common.images.image" ( dict "imageRoot" .Values.image "global" .Values.global ) | quote }}
          {{- with .Values.image }}
          imagePullPolicy: {{ .pullPolicy }}
          {{- end }}
          env:
            - name: "SELF_SERVICE_DB_SECRET"
              valueFrom:
                secretKeyRef:
                  {{- if (include "umc-server.postgresql.auth.credentialSecret.name" . ) }}
                  name: {{ include "umc-server.postgresql.auth.credentialSecret.name" . | quote }}
                  key: {{ (include "umc-server.postgresql.auth.credentialSecret.key" . ) | quote }}
                  {{- else }}
                  name: {{ include "common.names.fullname" . }}
                  key: "db_password"
                  {{- end }}
            - name: "SELF_SERVICE_MEMCACHED_SECRET"
              valueFrom:
                secretKeyRef:
                  {{- if (include "umc-server.memcached.auth.credentialSecret.name" . ) }}
                  name: {{ include "umc-server.memcached.auth.credentialSecret.name" . | quote }}
                  key: {{ (include "umc-server.memcached.auth.credentialSecret.key" . ) | quote }}
                  {{- else }}
                  name: {{ include "common.names.fullname" . }}
                  key: "memcached_password"
                  {{- end }}
          envFrom:
            - configMapRef:
                name: {{ include "common.names.fullname" . }}
          volumeMounts:
            {{- if .Values.extraVolumeMounts }}
            {{ toYaml .Values.extraVolumeMounts | nindent 12 }}
            {{- end }}
            {{- if or .Values.mountUcr .Values.global.nubusDeployment }}
            - name: "config-map-ucr-defaults"
              mountPath: "/etc/univention/base-defaults.conf"
              subPath: "base.conf"
            {{- end }}
            {{- if (include "umc-server.configMapUcr" .) }}
            - name: "config-map-ucr"
              mountPath: "/etc/univention/base.conf"
              subPath: "base.conf"
            {{- end }}
            {{- if (include "umc-server.configMapUcrForced" .) }}
            - name: "config-map-ucr-forced"
              mountPath: "/etc/univention/base-forced.conf"
              subPath: "base.conf"
            {{- end }}
            {{- if and .Values.mountSecrets (not .Values.global.nubusDeployment) }}
            - name: "secrets"
              mountPath: "{{ .Values.umcServer.secretMountPath }}"
            {{- else }}
            {{- if (include "umc-server.ldap.credentialSecret.name" . ) }}
            - name: {{ printf "%s-volume" (include "umc-server.ldap.credentialSecret.name" . ) | quote }}
              mountPath: "{{ .Values.umcServer.secretMountPath }}/ldap_secret"
              subPath: {{ .Values.ldap.credentialSecret.ldapPasswordKey | quote }}
              readOnly: true
            - name: {{ printf "%s-volume" (include "umc-server.ldap.credentialSecret.name" . ) | quote }}
              mountPath: "{{ .Values.umcServer.secretMountPath }}/machine_secret"
              subPath: {{ .Values.ldap.credentialSecret.machinePasswordKey | quote }}
              readOnly: true
            {{- end }}
            {{- if (include "umc-server.smtp.credentialSecret.name" . ) }}
            - name: {{ printf "%s-volume" (include "umc-server.smtp.credentialSecret.name" . ) | quote }}
              mountPath: "{{ .Values.umcServer.secretMountPath }}/smtp_secret"
              subPath: {{ .Values.smtp.credentialSecret.key | quote }}
              readOnly: true
            {{- end }}
            {{- if (include "umc-server.postgresql.auth.credentialSecret.name" . ) }}
            - name: {{ printf "%s-volume" (include "umc-server.postgresql.auth.credentialSecret.name" . ) | quote }}
              mountPath: "{{ .Values.umcServer.secretMountPath }}/db_password"
              subPath: {{ .Values.postgresql.auth.credentialSecret.key | quote }}
              readOnly: true
            {{- end }}
            {{- if (include "umc-server.memcached.auth.credentialSecret.name" . ) }}
            - name: {{ printf "%s-volume" (include "umc-server.memcached.auth.credentialSecret.name" . ) | quote }}
              mountPath: "{{ .Values.umcServer.secretMountPath }}/memcached_password"
              subPath: {{ .Values.memcached.auth.credentialSecret.key | quote }}
              readOnly: true
            {{- end }}
            {{- if (include "umc-server.ldap.tlsSecret.name" . ) }}
            - name: {{ printf "%s-volume" (include "umc-server.ldap.tlsSecret.name" . ) | quote }}
              mountPath: "{{ .Values.umcServer.secretMountPath }}/ca_cert"
              subPath: {{ .Values.ldap.tlsSecret.caCertKey | quote }}
              readOnly: true
            {{- end }}
            {{- if (include "umc-server.ldap.tlsSecret.name" . ) }}
            - name: {{ printf "%s-volume" (include "umc-server.ldap.tlsSecret.name" . ) | quote }}
              mountPath: "{{ .Values.umcServer.secretMountPath }}/cert_pem"
              subPath: {{ .Values.ldap.tlsSecret.certificateKey | quote }}
              readOnly: true
            {{- end }}
            {{- if (include "umc-server.ldap.tlsSecret.name" . ) }}
            - name: {{ printf "%s-volume" (include "umc-server.ldap.tlsSecret.name" . ) | quote }}
              mountPath: "{{ .Values.umcServer.secretMountPath }}/private_key"
              subPath: {{ .Values.ldap.tlsSecret.privateKeyKey | quote }}
              readOnly: true
            {{- end }}
            {{- end }}
          ports:
            {{- range $key, $value := .Values.service.ports }}
            - name: {{ $key }}
              containerPort: {{ $value.containerPort }}
              protocol: {{ $value.protocol }}
            {{- end }}
          {{- if .Values.probes.liveness.enabled }}
          livenessProbe:
            tcpSocket:
              port: http
            initialDelaySeconds: {{ .Values.probes.liveness.initialDelaySeconds }}
            timeoutSeconds: {{ .Values.probes.liveness.timeoutSeconds }}
            periodSeconds: {{ .Values.probes.liveness.periodSeconds }}
            failureThreshold: {{ .Values.probes.liveness.failureThreshold }}
            successThreshold: {{ .Values.probes.liveness.successThreshold }}
          {{- end }}
          {{- if .Values.probes.readiness.enabled }}
          readinessProbe:
            tcpSocket:
              port: http
            initialDelaySeconds: {{ .Values.probes.readiness.initialDelaySeconds }}
            timeoutSeconds: {{ .Values.probes.readiness.timeoutSeconds }}
            periodSeconds: {{ .Values.probes.readiness.periodSeconds }}
            failureThreshold: {{ .Values.probes.readiness.failureThreshold }}
            successThreshold: {{ .Values.probes.readiness.successThreshold }}
          {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        {{- if .Values.extraVolumes }}
        {{ toYaml .Values.extraVolumes | nindent 8 }}
        {{- end }}
        {{- if or .Values.mountUcr .Values.global.nubusDeployment }}
        - name: "config-map-ucr-defaults"
          configMap:
            name: {{ include "umc-server.configMapUcrDefaults" . | quote }}
        {{- end }}
        {{- if (include "umc-server.configMapUcr" .) }}
        - name: "config-map-ucr"
          configMap:
            name: {{ include "umc-server.configMapUcr" . | quote }}
        {{- end }}
        {{- if (include "umc-server.configMapUcrForced" .) }}
        - name: "config-map-ucr-forced"
          configMap:
            name: {{ include "umc-server.configMapUcrForced" . | quote }}
        {{- end }}
        {{- if and .Values.mountSecrets (not .Values.global.nubusDeployment) }}
        - name: "secrets"
          secret:
            secretName: {{ include "common.names.fullname" . | quote }}
        {{- else }}
        {{- if (include "umc-server.ldap.credentialSecret.name" . ) }}
        - name: {{ printf "%s-volume" (include "umc-server.ldap.credentialSecret.name" . ) | quote }}
          secret:
            secretName: {{ (include "umc-server.ldap.credentialSecret.name" . ) | quote }}
        {{- end }}
        {{- if (include "umc-server.smtp.credentialSecret.name" . ) }}
        - name: {{ printf "%s-volume" (include "umc-server.smtp.credentialSecret.name" . ) | quote }}
          secret:
            secretName: {{ (include "umc-server.smtp.credentialSecret.name" . ) | quote }}
        {{- end }}
        {{- if (include "umc-server.postgresql.auth.credentialSecret.name" . ) }}
        - name: {{ printf "%s-volume" (include "umc-server.postgresql.auth.credentialSecret.name" . ) | quote }}
          secret:
            secretName: {{ (include "umc-server.postgresql.auth.credentialSecret.name" . ) | quote }}
        {{- end }}
        {{- if (include "umc-server.memcached.auth.credentialSecret.name" . ) }}
        - name: {{ printf "%s-volume" (include "umc-server.memcached.auth.credentialSecret.name" . ) | quote }}
          secret:
            secretName: {{ (include "umc-server.memcached.auth.credentialSecret.name" . ) | quote }}
        {{- end }}
        {{- if (include "umc-server.ldap.tlsSecret.name" . ) }}
        - name: {{ printf "%s-volume" (include "umc-server.ldap.tlsSecret.name" . ) | quote }}
          secret:
            secretName: {{ (include "umc-server.ldap.tlsSecret.name" . ) | quote }}
        {{- end }}
        {{- end }}
